import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { ConnectedRouter, push } from 'react-router-redux';
import { Switch, Route } from 'react-router-dom';
import { Segment } from 'semantic-ui-react';

import Home from './containers/Home';
import Games from './containers/Games';
import NewGame from './containers/NewGame';
import GameDetail from './containers/GameDetail';
import GameScene from './containers/GameScene';
import CharacterDetail from './containers/CharacterDetail';
import Characters from './containers/Characters';
import NewCharacter from './containers/NewCharacter';
import RootContainer from './RootContainer';
import TopNav from './components/TopNav';

import * as actionTypes from './actionTypes';

class App extends Component {
  static propTypes = {
    history: PropTypes.object,
    login: PropTypes.func,
    loggedIn: PropTypes.bool,
    user: PropTypes.object,
    pushRoute: PropTypes.func
  }
  componentWillMount () {
    this.props.login();
  }

  onClickTopNavLink = (link) => {
    this.props.pushRoute(link);
  }

  render () {
    const { loggedIn, user, pathname, history } = this.props;
    return (
      <div>
        <RootContainer loggedIn={loggedIn}>
          <TopNav
            user={user}
            onItemClicked={this.onClickTopNavLink}
          />
          <ContentContainer fullScreen={pathname && pathname.indexOf('/scene') === -1}>
            <ConnectedRouter history={history}>
              <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/games' component={Games} />
                <Route exact path='/games/:id' component={GameDetail} />
                <Route exact path='/new-game' component={NewGame} />
                <Route exact path='/characters' component={Characters} />
                <Route exact path='/characters/:id' component={CharacterDetail} />
                <Route exact path='/new-character' component={NewCharacter} />
                <Route exact path='/scene' component={GameScene} />
              </Switch>
            </ConnectedRouter>
          </ContentContainer>
        </RootContainer>
      </div>
    );
  }
}

const ContentContainer = (props) => {
  return props.fullScreen ? <Segment
    style={{
      marginBottom: 20,
      minHeight: 800
    }}
  > {props.children}
  </Segment>
    : props.children;
};

const mapStateToProps = (state) => ({
  pathname: state.router.location && state.router.location.pathname,
  loggedIn: state.auth.loggedIn,
  user: state.auth.user
});

const mapDispatchToProps = (dispatch) => ({
  login: () => dispatch({type: actionTypes.LOGIN_REQUEST}),
  pushRoute: (link) => dispatch(push(link))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
