import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import createHistory from 'history/createBrowserHistory';

import { routerMiddleware } from 'react-router-redux';
import logger from 'redux-logger';

import 'semantic-ui-css/semantic.min.css';
import registerServiceWorker from './registerServiceWorker';

import './index.css';
import App from './App';
import reducers from './reducers';
import mySaga from './sagas';

// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const routerMiddlewares = routerMiddleware(history);

const sagaMiddleware = createSagaMiddleware();
const middleware = [
  sagaMiddleware,
  routerMiddlewares,
  logger
];

const store = createStore(
  reducers,
  compose(
    applyMiddleware(...middleware)
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

sagaMiddleware.run(mySaga);

ReactDOM.render(
  <Provider store={store}>
    <App history={history} />
  </Provider>
  , document.getElementById('root'));

if (module.hot) {
  module.hot.accept('./App', () => {
    ReactDOM.render(
      <Provider store={store}>
        <App history={history} />
      </Provider>,
      document.getElementById('root')
    );
  });
}
registerServiceWorker();
