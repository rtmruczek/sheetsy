import React, { Component } from 'react';
import {Icon} from 'semantic-ui-react';
class Collapsible extends Component {
    state = {
      hidden: false
    }
    toggleHidden = () => {
      this.setState({
        hidden: !this.state.hidden
      });
    }
    render () {
      return (
        <div style={{position: 'relative'}}>
          <Icon
            style={{cursor: 'pointer', position: 'absolute', right: 0, top: 0}}
            onClick={this.toggleHidden}
            name={this.state.hidden ? 'minus' : 'plus'} />
          <div style={{
            display: this.state.hidden ? 'none' : 'block'
          }}>
            {this.props.children}
          </div>
        </div>
      );
    }
}

export default Collapsible;
