import React from 'react';
import { PropTypes } from 'prop-types';
import { Popup, Icon } from 'semantic-ui-react';

const TooltipIconLabel = (props) => {
  const myIcon = <Icon name={props.name || 'help circle'} />;
  const triggerComponent =
  props.labelComponent
    ? React.cloneElement(props.labelComponent, {},
      <span>{props.labelComponent.props.children}{myIcon}</span>)

    : React.createElement('label', {},
      <span>{props.label}{myIcon}</span>);

  return (<Popup
    content={props.content}
    trigger={triggerComponent}
  />);
};

TooltipIconLabel.propTypes = {
  labelComponent: PropTypes.element,
  label: PropTypes.string,
  name: PropTypes.string,
  content: PropTypes.string
};

export default TooltipIconLabel;
