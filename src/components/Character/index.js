import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Collapsible from '../Collapsible';
import { StressList } from '../CharacterForm';
import { Card, List, Header } from 'semantic-ui-react';
import './styles.css';

const Character = {};
Character.Card = (props) => {
  const { item } = props;
  return (
    <Link to={`/characters/${item.id}`}>
      <Card
        className='characterCard'
        key={item.id}>
        <Card.Content>
          <Card.Header>{item.name}</Card.Header>
        </Card.Content>
      </Card>
    </Link>);
};

Character.SceneCard = (props) => {
  const { item, size, x, y } = props;
  return (
    <Card
      style={{
        width: '200px',
        position: 'absolute',
        left: x,
        top: y
      }}
      className='sceneCard'
      key={item.id}
    >
      <Card.Content>
        <Card.Header>{item.name}</Card.Header>
        <Header as='h5'>Aspects</Header>
        <List animated selection>
          {
            item.aspects && item.aspects.map(aspect => <List.Item key={aspect.id}>{aspect.value}</List.Item>)
          }
        </List>
        <Header as='h5'>Physical Stress</Header>
        <StressList active skill='Physique' skills={item.skills} />
        <Header as='h5'>Mental Stress</Header>
        <StressList active skill='Will' skills={item.skills} />
      </Card.Content>
    </Card>
  );
};

Character.Group = (props) => {
  return (
    <Card.Group className='characterList'>
      {
        props.items.map(item => <Character.Card {...props} key={item.id} item={item} />)
      }
    </Card.Group>
  );
};
Character.Card.propTypes = {
  item: PropTypes.object
};

Character.Group.propTypes = {
  items: PropTypes.array
};
Character.Group.defaultProps = {
  items: []
};

export default Character;
