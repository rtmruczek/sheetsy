import React, {Component} from 'react';
import { Modal } from 'semantic-ui-react';
import PropTypes from 'prop-types';

class Skill extends Component {
  state = {
    open: false
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.skill) {
      this.setState({
        open: true
      });
    }
  }
  handleClose = () => {
    this.setState({
      open: false
    });
  }
  render () {
    const skill = this.props.skill || {};
    return (
      <Modal
        open={this.state.open}
        onClose={this.handleClose}
      >
        <Modal.Header>{skill.name}</Modal.Header>
        <Modal.Content>
          <p style={{
            fontSize: 16
          }}>{skill.description}</p>
          <SkillAction skill={skill} actionField='overcome' actionName='Overcome' />
          <SkillAction skill={skill} actionField='createAdvantage' actionName='Create an Advantage' />
          <SkillAction skill={skill} actionField='attack' actionName='Attack' />
          <SkillAction skill={skill} actionField='defend' actionName='Defend' />
        </Modal.Content>
      </Modal>);
  }
}
Skill.propTypes = {
  skill: PropTypes.object
};
Skill.defaultProps = {
  skill: {}
};

const SkillAction = (props) => {
  const {skill, actionField, actionName} = props;
  return (
    skill[actionField]
      ? <div>
        <h4>{ actionName }</h4>
        <p style={{
          margin: '0 0 1em'
        }}
        >{skill[actionField]}</p>
      </div>
      : <h4>{ skill.name } is not typically used for { actionName } actions.</h4>
  );
};

SkillAction.propTypes = {
  skill: PropTypes.object,
  actionField: PropTypes.string,
  actionName: PropTypes.string
};

export default Skill;
