import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import { DragSource } from 'react-dnd';

const buttonStyle = (fullWidth) => ({
  marginBottom: fullWidth ? 0 : 4,
  marginRight: 4,
  width: fullWidth ? '100%' : 'auto',
  display: 'inline-block',
  cursor: 'move'
});
class SkillButton extends Component {
  static propTypes = {
    value: PropTypes.number,
    connectDragSource: PropTypes.func,
    fullWidth: PropTypes.bool,
    skill: PropTypes.object,
    selectSkill: PropTypes.func
  }
  selectSkill = () => {
    this.props.selectSkill(this.props.skill);
  }
  render () {
    const { skill, value, connectDragSource, fullWidth } = this.props;
    const comp = (<Button
      fluid={fullWidth}
      style={{margin: 0, cursor: 'move'}}
      key={skill.name}
      onClick={this.selectSkill}
    >{skill.name}
      {value ? ` (+${value})` : ''}
    </Button>);

    return connectDragSource
      ? connectDragSource(
        <div style={buttonStyle(fullWidth)}>{comp}</div>)
      : comp;
  }
}

const collect = (connect, monitor) => {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
};

const dragSource = {
  beginDrag (props) {
    props.beginDragging({name: props.skill.name, fromContainer: props.fromContainer});
    return {name: props.skill.name};
  }
};

export default SkillButton;

const DraggableSkillButton = DragSource('SkillButton', dragSource, collect)(SkillButton);
export { DraggableSkillButton };
