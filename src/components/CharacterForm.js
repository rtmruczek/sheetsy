import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Divider, Button, Checkbox } from 'semantic-ui-react';
import CharacterSkills from './CharacterSkills';
import TooltipIconLabel from './TooltipIconLabel';
import IncrementingList from './IncrementingList';
import './styles.css';
import Collapsible from './Collapsible';

class CharacterForm extends Component {
  static propTypes = {
    addCharacter: PropTypes.func,
    isNewObject: PropTypes.bool,
    character: PropTypes.object,
    updateField: PropTypes.func
  }
  state = {
    valid: true
  }
  updateField = (evt, element) => {
    const { name, value } = element;
    this.props.updateField(name, value);
  }
  render () {
    const { isNewObject, character } = this.props;
    if (!isNewObject && !character.id) {
      return <div>Loading</div>;
    }
    return (
      <div>
        <div style={{display: 'flex', justifyContent: 'space-between'}}>
          <h1 style={{margin: 0}}>New Character</h1>
          <Button
            disabled={!this.state.valid}
            onClick={this.props.addCharacter}
            content='Save' color='green' icon='check' labelPosition='left' />
        </div>
        <Divider />
        <Form>
          <Collapsible>
            <div style={{
              display: 'flex'
            }}>
              <div style={{flex: 1, marginRight: 10}}>
                <Form.Input
                  name='name'
                  defaultValue={this.props.character.name}
                  onChange={this.updateField}
                  label='Name'
                  placeholder='Captain Danny Savage'
                />
                <Form.Input
                  name='highConcept'
                  defaultValue={this.props.character.highConcept}
                  onChange={this.updateField}
                  label={<TooltipIconLabel
                    label='High Concept'
                    content="This character's overarching theme"
                  />}
                  type='input'
                />
                <Form.Input
                  name='trouble'
                  onChange={this.updateField}
                  label={<TooltipIconLabel
                    label='Trouble'
                    content="This character's main problem in life"
                  />}
                  type='input'
                />
                <h3>Aspects</h3>
                <IncrementingList
                  name='aspects'
                  items={this.props.character.aspects}
                  onChange={this.updateField}
                />
              </div>
              <div style={{flex: 1, marginLeft: 10}}>
                <h3>Consequences</h3>
                <IncrementingList
                  name='consequences'
                  items={this.props.character.consequences}
                  onChange={this.updateField}
                />
                <Divider />
                <div className='characterStress'>
                  <div style={{flex: 1}}>
                    <TooltipIconLabel
                      labelComponent={<h4>Physical Stress</h4>}
                      content='Physical stress helps you avoid physical harm. Add more points to Physique to increase this.'
                    />
                    <StressList
                      skill='Physique'
                      skills={this.props.character.skills}
                    />
                  </div>
                  <div style={{flex: 1}}>
                    <TooltipIconLabel
                      labelComponent={<h4>Mental Stress</h4>}
                      content='Mental stress helps you avoid mental harm. Add more points to Will to increase this.'
                    />
                    <StressList
                      skill='Will'
                      skills={this.props.character.skills}
                    />
                  </div>
                </div>
              </div>
            </div>
          </Collapsible>
          <Divider />
          <div style={{display: 'flex'}}>
            <div style={{flex: 1}}>
              <CharacterSkills
                skills={this.props.character.skills}
                onChange={this.updateField}
              />
            </div>
            <div style={{flex: 1}}>
              <h2>Stunts</h2>
            </div>
          </div>
        </Form>
      </div>
    );
  }
}

const StressList = (props) => {
  const skill = props.skills.filter(sk => sk.name === props.skill);
  const valueToStress = {
    1: 3,
    2: 3,
    3: 4,
    4: 4
  };

  const numOfBoxes = skill.length === 1 ? valueToStress[skill[0].value] : 2;
  const checkboxes = new Array(numOfBoxes).fill();

  return (
    <div>
      { checkboxes.map((c, index) => (
        <Checkbox
          style={{marginRight: 20}}
          key={`checkbox/${index}`} disabled={!props.active} label={index + 1} />
      ))
      }
    </div>
  );
};
StressList.propTypes = {
  skills: PropTypes.array,
  skill: PropTypes.string,
  active: PropTypes.bool
};
StressList.defaultProps = {
  skills: []
};

export { StressList };
export default CharacterForm;
