import React, { Component } from 'react';
import { Header } from 'semantic-ui-react';
import { EditableText } from '@blueprintjs/core';
import './styles.css';

const Aspect = {};

Aspect.List = (props) => (
  <div className='aspect-list'>
    {props.children}
  </div>
);

Aspect.Card = (props) => {
  const { id, onEdit, ...rest } = props;
  const onConfirm = (evt) => {
    onEdit({id, name: evt});
  };
  return (<div {...rest} className='aspect-card'>
    <h4><EditableText placeholder='Edit Aspect Name...' onConfirm={onConfirm} /></h4>
    {/* <Header as='h4' style={{color: '#333'}}>New Aspect</Header> */}
  </div>);
};

export default Aspect;
