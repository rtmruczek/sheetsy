import React, { Component } from 'react';
import { Input, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import uuidv1 from 'uuid/v1';
import { isEqual } from 'lodash';
import { updatePureListWithMixin } from '../utils/listUtils';

class IncrementingList extends Component {
  static propTypes = {
    itemType: PropTypes.string,
    items: PropTypes.array,
    onChange: PropTypes.func,
    name: PropTypes.string
  }
  static defaultProps = {
    items: []
  }
  constructor (props) {
    super(props);
    const defaultValue = {value: ''};
    this.state = {
      items: props.items
        .map(item => {
          return !item.id
            ? { ...item, ...{id: uuidv1()} }
            : item;
        })
        .concat({...defaultValue, id: uuidv1()}) || [],
      defaultValue
    };
  }
  componentWillReceiveProps (nextProps) {
    const { defaultValue } = this.state;
    this.setState({
      items: nextProps.items.concat({...defaultValue, id: uuidv1()}) || []
    });
  }
  onChange = (index, value) => {
    const { items, defaultValue } = this.state;
    let newItems = updatePureListWithMixin(items, index, value);
    if (index === items.length - 1) {
      newItems = newItems.concat({...defaultValue, id: uuidv1()});
    }
    this.setState({
      items: newItems
    });
    this.props.onChange(null, {name: this.props.name, value: newItems.filter(v => !isEqual(v.value, defaultValue.value))});
  }
  onDelete = (index) => {
    const { items, defaultValue } = this.state;
    const newItems = [
      ...items.slice(0, index),
      ...items.slice(index + 1)
    ];
    this.setState({
      items: newItems
    });
    this.props.onChange(null, {name: this.props.name, value: newItems.filter(v => !isEqual(v.value, defaultValue.value))});
  }
  render () {
    return (
      this.state.items.map((item, index) => {
        switch (this.props.itemType) {
          case 'string':
          default:
            return (
              <IncrementingList.Input
                onChange={this.onChange}
                onDelete={this.onDelete}
                item={item}
                index={index}
                key={`inputList/${item.id}`} />
            );
        }
      })
    );
  }
}

IncrementingList.Input = (props) => {
  const onChange = (evt) => {
    const { value } = evt.target;
    props.onChange(props.index, {value});
  };
  const onDelete = () => {
    props.onDelete(props.index);
  };
  return (
    <div style={{position: 'relative'}}>
      <Input
        onChange={onChange}
        defaultValue={props.item.value}
        fluid />
      {
        props.item.value &&
        <Icon
          style={{
            position: 'absolute',
            right: 0,
            top: 0
          }}
          onClick={onDelete}
          name='delete'
        />
      }
    </div>
  );
};
IncrementingList.Input.propTypes = {
  onChange: PropTypes.func,
  index: PropTypes.number,
  onDelete: PropTypes.func,
  item: PropTypes.object
};

export default IncrementingList;
