import React, { Component } from 'react';
import { Input, Menu, Icon } from 'semantic-ui-react';
import { PropTypes } from 'prop-types';

class TopNav extends Component {
  static propTypes = {
    user: PropTypes.object,
    onItemClicked: PropTypes.func
  }

  state = { activeItem: 'home' }
  handleItemClick = (e, { name }) => {
    this.props.onItemClicked(name);
    this.setState({ activeItem: name });
  }

  render () {
    const { activeItem } = this.state;
    const { user } = this.props;

    return (
      <Menu pointing>
        <Menu.Item name='/' content='Home' active={activeItem === '/'} onClick={this.handleItemClick} />
        <Menu.Item name='/games' active={activeItem === '/games'} onClick={this.handleItemClick} />
        <Menu.Item name='/characters' active={activeItem === '/characters'} onClick={this.handleItemClick} />
        <Menu.Item name='/scene' active={activeItem === '/scene'} onClick={this.handleItemClick} />
        <Menu.Menu position='right'>
          <Menu.Item onClick={() => {}}>
            <Icon name='user' />
            {user.email}
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  }
}

export default TopNav;
