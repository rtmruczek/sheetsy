import React, { Component } from 'react';
import { Divider } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { findIndex, find, flatten } from 'lodash';
import { DragDropContextProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import { updatePureList } from '../utils/listUtils';
import test from '../test.json';
import Skill from './Skill';
import { DraggableSkillButton } from './SkillButton';
import { SkillContainerRow } from './SkillContainer';

class CharacterSkills extends Component {
  static propTypes = {
    skills: PropTypes.array,
    onChange: PropTypes.func
  }
  constructor (props) {
    super(props);
    const getSkillSlotsFromSkills = (skills, value) => {
      const filledskills = skills.filter(skill => skill.value === value);
      const leftoverSlots = 4 - value + 1 - filledskills.length;
      const leftovers = new Array(leftoverSlots).fill('');
      return filledskills.concat(leftovers);
    };
    this.state = {
      dragging: {
        name: null,
        fromContainer: false
      },
      selectedSkill: null,
      skills: [{
        value: 4,
        name: 'Excellent',
        slots: props.skills ? getSkillSlotsFromSkills(props.skills, 4) : Array(1).fill('')
      }, {
        value: 3,
        name: 'Great',
        slots: props.skills ? getSkillSlotsFromSkills(props.skills, 3) : Array(2).fill('')
      }, {
        value: 2,
        name: 'Good',
        slots: props.skills ? getSkillSlotsFromSkills(props.skills, 2) : Array(3).fill('')
      }, {
        value: 1,
        name: 'Fair',
        slots: props.skills ? getSkillSlotsFromSkills(props.skills, 1) : Array(4).fill('')
      }]
    };
  }

  selectSkill = (selectedSkill) => this.setState({selectedSkill})

  beginDragging = ({name, fromContainer}) => {
    this.setState({
      selectedSkill: null,
      dragging: {
        name,
        fromContainer
      }
    });
  }
  getNewDropState = (rowIndex, slotIndex, newValue) => {
    const oldRow = this.state.skills[rowIndex];
    const newRow = Object.assign({}, oldRow, {
      slots: updatePureList(oldRow.slots, slotIndex, newValue)
    });
    return {
      skills: updatePureList(this.state.skills, rowIndex, newRow)
    };
  }
  drop = (target) => {
    const { name, fromContainer } = this.state.dragging;
    const rowIndex = findIndex(this.state.skills, row => target.value === row.value);
    const skillObject = find(test.fate.skills, skill => skill.name === name);
    const slotIndex = target.index;
    const whatWasInTargetSlotBefore = this.state.skills[rowIndex].slots[slotIndex];

    let newState = this.getNewDropState(rowIndex, slotIndex, skillObject);
    this.setState(newState);
    if (fromContainer) {
      const fcRowIndex = findIndex(this.state.skills, row => fromContainer.value === row.value);
      const fcSlotIndex = fromContainer.index;
      newState = this.getNewDropState(fcRowIndex, fcSlotIndex, whatWasInTargetSlotBefore);
      this.setState(newState);
    }

    const skillsToPass = flatten(
      newState.skills.map(row => {
        return row.slots.map(slot => slot ? ({
          value: row.value,
          name: slot.name
        }) : null).filter(slot => slot);
      }).filter(row => row.length > 0));
    this.props.onChange(null, {name: 'skills', value: skillsToPass});
  }
  render () {
    const { selectedSkill, skills } = this.state;
    return (
      <div>
        <h2>Skills</h2>
        <DragDropContextProvider backend={HTML5Backend}>
          <div>
            {
              skills.map((row, index) => (
                <SkillContainerRow
                  selectSkill={this.selectSkill}
                  beginDragging={this.beginDragging}
                  onDrop={this.drop}
                  key={`skillContainer/${index}`} row={row} />
              ))
            }
            <Divider />
            {
              test.fate.skills.map(skill => (
                <DraggableSkillButton
                  beginDragging={this.beginDragging}
                  selectSkill={this.selectSkill}
                  skill={skill}
                  key={`draggableSkill/${skill.name}`} />
              ))
            }
            <Skill skill={selectedSkill} />
          </div>
        </DragDropContextProvider>
      </div>
    );
  }
}

export default CharacterSkills;
