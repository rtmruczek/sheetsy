import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DropTarget } from 'react-dnd';
import { DraggableSkillButton } from './SkillButton';

const containerStyle = {
  flex: 1,
  marginRight: 5
};
const style = {
  border: '1px dashed rgb(27, 106, 117)',
  borderRadius: 5,
  display: 'inline-block',
  textAlign: 'center',
  fontFamily: 'Lato',
  backgroundColor: '#cff4f9',
  padding: 7,
  flex: 1,
  marginRight: 5
};
const droppingStyle = {
  boxShadow: '0px 0px 3px 3px rgba(27, 106, 117, 0.25)',
  backgroundColor: 'lightblue'
};
class SkillContainer extends Component {
  state = { }
  render () {
    const { canDrop, isOver, connectDropTarget, name, value, skill, beginDragging, index, selectSkill } = this.props;
    const isActive = canDrop && isOver;
    const insideComponent = typeof skill === 'object'
      ? <div style={containerStyle}>
        <DraggableSkillButton
          fullWidth
          fromContainer={{value, index}}
          beginDragging={beginDragging}
          selectSkill={selectSkill}
          value={value}
          skill={skill} />
      </div>
      : <div style={{...style, ...isActive && droppingStyle}}>
        { skill.length > 0 ? skill : `${name} (+${value})`}
      </div>;

    return connectDropTarget(insideComponent);
  }
}
const boxTarget = {
  drop (props) {
    props.onDrop(props);
    return props;
  }
};
const dropSource = (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
});

const DroppableSkillContainer = DropTarget('SkillButton', boxTarget, dropSource)(SkillContainer);

const SkillContainerRow = (props) => (
  <div
    style={{
      display: 'flex',
      marginBottom: 10
    }}
  >
    {
      props.row.slots.map((slot, index) => (
        <DroppableSkillContainer
          skill={slot}
          beginDragging={props.beginDragging}
          selectSkill={props.selectSkill}
          onDrop={props.onDrop}
          name={props.row.name}
          value={props.row.value}
          index={index}
          key={`${props.row.name}_${index}`} />
      ))
    }
  </div>
);
SkillContainerRow.propTypes = {
  row: PropTypes.object,
  beginDragging: PropTypes.func,
  onDrop: PropTypes.func,
  selectSkill: PropTypes.func
};

export { DroppableSkillContainer, SkillContainerRow };
