import firebase from 'firebase';
import { firebaseAdd } from './utils';

export function getGames (userId) {
  return firebase.database().ref(`/users/${userId}/games`).once('value').then((snapshot) => {
    return snapshot.val();
  });
}

export function addGame (userId, game) {
  return firebaseAdd(userId, 'games', game);
}

export function deleteGame (userId, gameId) {
  const updates = {
    [`users/${userId}/games/${gameId}`]: null
  };
  return firebase.database().ref().update(updates);
}
