import firebase from 'firebase';

export function getAllUserData () {
  return firebase.database().ref(`/logged_in`).once('value').then((snapshot) => {
    return snapshot.val();
  });
}

export function getAllMyData (userId) {
  return firebase.database().ref(`/users/${userId}`).once('value').then((snapshot) => {
    return snapshot.val();
  });
}
