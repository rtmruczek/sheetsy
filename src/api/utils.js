import firebase from 'firebase';
import { eventChannel } from 'redux-saga';

export function firebaseAdd (userId, path, object) {
  const userRef = firebase.database().ref('users/' + userId);
  const isNewObject = !object || !object.id;
  const dates = isNewObject ? {
    createdAt: firebase.database.ServerValue.TIMESTAMP
  } : {
    modifiedAt: firebase.database.ServerValue.TIMESTAMP
  };
  const id = isNewObject ? userRef.child(path).push().key : object.id;
  const updates = {
    [`users/${userId}/${path}/${id}`]: {
      ...object,
      ...dates,
      id
    },
    [`logged_in/${path}/${userId}/${id}`]: {
      ...object,
      ...dates,
      id
    }
  };

  return firebase.database().ref().update(updates);
}

export function createSocketChannel (socket) {
  // `eventChannel` takes a subscriber function
  return eventChannel(emit => {
    const pingHandler = (event) => {
      emit(event.val() || {});
    };

    // setup the subscription
    socket.on('value', pingHandler);

    // the subscriber must return an unsubscribe function
    // this will be invoked when the saga calls `channel.close` method
    const unsubscribe = () => {
      socket.off('ping', pingHandler);
    };

    return unsubscribe;
  });
}
