import firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyC6CfXlhZ3D8l3JgsdKRSqWy8KYTImdSwY',
  authDomain: 'sheetsy-12e2a.firebaseapp.com',
  databaseURL: 'https://sheetsy-12e2a.firebaseio.com',
  projectId: 'sheetsy-12e2a',
  storageBucket: 'sheetsy-12e2a.appspot.com',
  messagingSenderId: '714480306680'
};

firebase.initializeApp(config);

export function login () {
  return new Promise((resolve, reject) => {
    const provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('profile');
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        resolve(Object.assign({},
          user.providerData[0], {
            uid: user.uid
          }
        ));
      } else {
        firebase.auth().signInWithRedirect(provider);
      }
    });
  });
}
