import firebase from 'firebase';
import {firebaseAdd} from './utils';

export function getCharacters (userId) {
  return firebase.database().ref(`/users/${userId}/characters`).once('value')
    .then(snapshot => snapshot.val());
}

export function watchCharacters (userId) {
  return firebase.database().ref(`/users/${userId}/characters`);
}

export function addCharacter (userId, character) {
  return firebaseAdd(userId, 'characters', character);
}
