import firebase from 'firebase';
import {firebaseAdd} from './utils';

export function getScenes (userId) {
  return firebase.database().ref(`/users/${userId}/scenes`).once('value')
    .then(snapshot => snapshot.val());
}

export function watchScenes (userId) {
  return firebase.database().ref(`/users/${userId}/scenes`);
}

export function watchScene (userId, sceneId) {
  return firebase.database().ref(`/users/${userId}/scenes/${sceneId}`);
}

export function addScene (userId, scene) {
  debugger;
  return firebaseAdd(userId, 'scenes', scene);
}
