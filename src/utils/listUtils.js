// @flow

const updatePureListWithMixin = (list: Array, index: number, value) => {
  return [
    ...list.slice(0, index),
    ...[{...list[index], ...value}],
    ...list.slice(index + 1)
  ];
};

const updatePureList = (list: Array, index: number, value) => {
  return [
    ...list.slice(0, index),
    ...[value],
    ...list.slice(index + 1)
  ];
};

export {
  updatePureList,
  updatePureListWithMixin
};
