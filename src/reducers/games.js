import * as actionTypes from '../actionTypes';
import { values, findIndex } from 'lodash';

const initialState = {
  mine: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    // case actionTypes.ADD_SCENE: {
    //   const gameId = action.payload;
    //   const index = findIndex(state.mine, g => g.id === gameId);
    //   if (index) {
    //     const newState = Object.assign({}, state, {
    //       mine: [
    //         ...state.mine.slice(0, index),
    //         ...[
    //           Object.assign({}, state.mine[index], {
    //             scenes: state.mine[index].scenes ? state.mine[index].scenes.concat({gameId}) : [{gameId}]
    //           })
    //         ],
    //         ...state.mine.slice(index + 1)
    //       ]
    //     });
    //     return newState;
    //   }
    //   return state;
    // }
    case actionTypes.RECEIVE_GAMES: return Object.assign({},
      state, {
        mine: values(action.payload)
      });
    default:
      return state;
  }
};
