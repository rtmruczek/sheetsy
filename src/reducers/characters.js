// @flow

import * as actionTypes from '../actionTypes';
import { values } from 'lodash';

const initialState = {
  mine: [],
  others: [],
  form: {
    // This acts as a memento of state changes. 
    // There shouldn't be anything in this initially, 
    // because we rely on UPDATE_FIELD actions to populate this with the necessary changes.
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.UPDATE_FIELD:
      return Object.assign({}, state, {
        form: Object.assign({}, state.form, {
          [action.payload.name]: action.payload.value
        })
      });
    case actionTypes.RECEIVE_ALL_USER_DATA:
      return {
        ...state,
        others: action.payload.characters
      };
    case actionTypes.RECEIVE_CHARACTERS: return Object.assign({},
      state, {
        mine: values(action.payload)
      });
    case actionTypes.RECEIVE_ALL_MY_DATA: return Object.assign({},
      state, {
        mine: values(action.payload.characters)
      });
    case '@@router/LOCATION_CHANGE': return {
      ...state,
      form: {}
    };
    default:
      return state;
  }
};
