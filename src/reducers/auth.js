import * as actionTypes from '../actionTypes';

export const initialState = {
  user: {
    email: null
  },
  loggedIn: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOGIN_SUCCESS:
      return Object.assign({}, state, {
        user: action.payload
      });
    case actionTypes.BOOTSTRAP_COMPLETE:
      return Object.assign({}, state, {
        loggedIn: true
      });
    default:
      return state;
  }
};
