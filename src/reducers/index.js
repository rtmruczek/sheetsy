import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import characters from './characters';
import auth from './auth';
import games from './games';
import scene from './scene';

export default combineReducers({
  auth,
  characters,
  games,
  scene,
  router: routerReducer
});
