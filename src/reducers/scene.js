import { find, findIndex, values } from 'lodash';
import { updatePureList } from '../utils/listUtils';
import uuidv1 from 'uuid/v1';

const initialState = {
  loading: true,
  mine: [],
  activeScene: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.RECEIVE_ALL_MY_DATA: return Object.assign({},
      state, {
        mine: values(action.payload.scenes)
      });
    case actionTypes.SET_ACTIVE_SCENE: {
      const activeScene = find(state.mine, scene => scene.id === action.payload);
      return Object.assign({}, state, {
        activeScene: Object.assign({}, activeScene, {
          aspects: state.activeScene.aspects || [],
          characters: state.activeScene.characters || []
        }),
        loading: false
      });
    }
    case actionTypes.ADD_ASPECT_TO_SCENE: {
      return Object.assign({}, state, {
        activeScene: {
          ...state.activeScene,
          ...{
            aspects: state.activeScene.aspects.concat({
              id: uuidv1(),
              name: ''
            })
          }}
      });
    }
    case actionTypes.ADD_CHARACTER_TO_SCENE: {
      return Object.assign({}, state, {
        activeScene: {
          ...state.activeScene,
          ...{
            characters: state.activeScene.characters.concat({
              characterId: action.payload.id
            })
          }
        }
      });
    }
    case actionTypes.UPDATE_ASPECT_IN_SCENE: {
      const indexToUpdate = findIndex(state.activeScene.aspects, a => a.id === action.payload.id);
      const newAspects = updatePureList(state.activeScene.aspects, indexToUpdate, action.payload);
      return Object.assign({}, state, {
        activeScene: {
          ...state.activeScene,
          ...{
            aspects: newAspects
          }
        }
      });
    }
    default:
      return state;
  }
};
