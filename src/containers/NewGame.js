import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import { Button, Dropdown, Form, Divider } from 'semantic-ui-react';
import { connect } from 'react-redux';
import * as actionTypes from '../actionTypes';

class NewGame extends Component {
  static propTypes = {
    submitGame: PropTypes.func,
    history: PropTypes.object
  }
  state = {
    title: '',
    type: '',
    description: ''
  }

  submitGame = () => {
    this.props.submitGame(this.state);
    this.props.history.push('games');
  }

  updateField = (evt, {name, value}) => {
    this.setState({
      [name]: value
    });
  }

  render () {
    return (
      <div>
        <h2>New Game</h2>
        <Form>
          <Form.Input
            name='title'
            label='Title'
            placeholder='Title'
            onChange={this.updateField}
          />
          <Form.Select
            placeholder='Type'
            label='Type'
            name='type'
            onChange={this.updateField}
            options={[{key: 'fate', value: 'fate', text: 'Fate'}]}
          />
          <Form.TextArea
            placeholder='Description'
            label='Description'
            name='description'
            onChange={this.updateField}
          />
          <Divider />
          <Button onClick={this.submitGame}type='submit'>Submit</Button>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({
  submitGame: (game) => dispatch({
    type: actionTypes.CREATE_NEW_GAME,
    payload: game
  })
});
export default connect(mapStateToProps, mapDispatchToProps)(NewGame);
