import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Header, List, Modal, Tab } from 'semantic-ui-react';
import NewCharacter from '../../containers/NewCharacter';

class GameSceneModal extends Component {
    state = { }
    static propTypes = {
      characters: PropTypes.array,
      isModalOpen: PropTypes.bool,
      onAddCharacter: PropTypes.func,
      onClose: PropTypes.func
    }
    panes = [{
      menuItem: 'Existing Characters',
      render: () => (
        <div>
          <Header dividing as='h3'>My Characters</Header>
          <List selection size='large'>
            { this.props.characters.map(character => (
              <List.Item
                onClick={() => this.props.onAddCharacter(character)}
                key={character.id}>
                <List.Content>
                  <List.Header>{character.name}</List.Header>
                </List.Content>
              </List.Item>
            ))
            }
          </List>
          <Header dividing as='h3'>Other Characters</Header>
        </div>
      )
    }, {
      menuItem: 'New Character',
      render: () => <NewCharacter />
    }];
    render () {
      return (
        <Modal
          size='large'
          onClose={this.props.onClose}
          open={this.props.isModalOpen}>
          <Modal.Content style={{minHeight: 800}}>
            <Tab menu={{ secondary: true, pointing: true }} panes={this.panes} />
          </Modal.Content>
        </Modal>
      );
    }
}

export default GameSceneModal;
