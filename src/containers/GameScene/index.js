import React, { Component } from 'react';
import { Menu, Loader } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { sortBy, first, find } from 'lodash';

import uuidv1 from 'uuid/v1';

import Character from '../../components/Character';
import Aspect from '../../components/Aspect';
import GameSceneModal from './GameSceneModal';
import MouseProps from './MouseProps';

class GameScene extends Component {
  constructor (props) {
    super(props);
    this.state = {
      id: uuidv1(),
      sceneCharacters: [],
      sceneAspects: [],
      mouseState: {}
    };
  }
  componentWillMount () {
    if (!this.props.match.params.id && this.props.myScenes.length === 0) {
      this.props.createScene();
    } else {
      const lastModifiedScene = first(sortBy(this.props.myScenes, scene => new Date(scene.modifiedAt)));
      this.props.setActiveScene(lastModifiedScene.id);
    }
  }
  componentDidMount () {
    document.body.className = 'body-grid';
  }

  componentWillUnmount () {
    document.body.className = '';
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.myScenes.length > 0 && !this.props.activeScene.id) {
      const lastModifiedScene = first(sortBy(nextProps.myScenes, scene => new Date(scene.modifiedAt)));
      this.props.setActiveScene(lastModifiedScene.id);
    }

    // populate the chars with all data from master char records
    const stateCharacters = nextProps.activeScene.characters
      .map((activeChar) => {
        const alreadyActiveChar = find(this.state.sceneCharacters, (char) => char.id === activeChar.characterId);
        if (alreadyActiveChar) {
          return alreadyActiveChar;
        }
        return {
          ...find(nextProps.characters, (char) => char.id === activeChar.characterId),
          position: {
            x: 0, y: 0
          },
          oldPosition: {
            x: 0, y: 0
          }
        };
      });
    this.setState({
      sceneCharacters: stateCharacters,
      sceneAspects: nextProps.activeScene.aspects
    });
  }
  toggleCharacterModal = () => {
    this.setState({
      characterModalOpen: !this.state.characterModalOpen
    });
  }
  addCharacterToScene = (character) => {
    this.props.addCharacter(character);
    this.toggleCharacterModal();
  }
  handleClose = () => {
    this.setState({
      characterModalOpen: false
    });
  }
  onMouseUp = (mouseState) => {
    let newState = {
      ...this.state,
      mouseState,
      sceneCharacters: this.state.sceneCharacters.map((sceneChar) => {
        return {
          ...sceneChar,
          oldPosition: {
            x: sceneChar.position.x,
            y: sceneChar.position.y
          }
        };
      })
    };
    this.setState(newState);
  }
  onMouseMoveDown = (mouseState) => {
    let newState = {
      ...this.state,
      mouseState,
      sceneCharacters: this.state.sceneCharacters.map((sceneChar) => {
        return {
          ...sceneChar,
          position: {
            x: sceneChar.oldPosition.x + mouseState.backgroundDrag.screenOffsetX - mouseState.backgroundDrag.startScreenOffsetX,
            y: sceneChar.oldPosition.y + mouseState.backgroundDrag.screenOffsetY - mouseState.backgroundDrag.startScreenOffsetY
          }
        };
      })
    };
    this.setState(newState);
  }
  render () {
    return (
      <div>
        <Loader inverted active={this.props.loading}>Loading new scene...</Loader>
        { !this.props.loading &&
          <MouseProps
            onMouseMoveDown={this.onMouseMoveDown}
            onMouseUp={this.onMouseUp}
          >
            <div>
              <pre>
                { JSON.stringify(this.state.mouseState) }
              </pre>
              <Menu size='large' vertical>
                <Menu.Item onClick={this.props.addAspect} icon='plus' name='Add aspect to scene' />
                <Menu.Item onClick={this.toggleCharacterModal} icon='id card outline' name='Add Character to Scene' />
              </Menu>
              <Aspect.List>
                {
                  this.state.sceneAspects.map(aspect =>
                    <Aspect.Card
                      key={aspect.id}
                      className='moveable-object'
                      style={{
                        cursor: 'move'
                      }}
                      onEdit={this.props.updateAspect} id={aspect.id} />
                  )
                }
              </Aspect.List>
              <div style={{position: 'relative'}}>
                {
                  this.state.sceneCharacters.map(char => (
                    <Character.SceneCard
                      x={char.position.x}
                      y={char.position.y}
                      className='moveable-object'
                      key={char.id} item={char} />
                  ))
                }
              </div>
              <GameSceneModal
                onAddCharacter={this.addCharacterToScene}
                onClose={this.handleClose}
                isModalOpen={this.state.characterModalOpen}
                characters={this.props.characters} />

            </div>
          </MouseProps>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  characters: state.characters.mine,
  myScenes: state.scene.mine,
  loading: state.scene.loading,
  activeScene: state.scene.activeScene
});

const mapDispatchToProps = (dispatch) => ({
  addAspect: () => dispatch({type: actionTypes.ADD_ASPECT_TO_SCENE}),
  addCharacter: (payload) => dispatch({type: actionTypes.ADD_CHARACTER_TO_SCENE, payload}),
  updateAspect: (payload) => dispatch({type: actionTypes.UPDATE_ASPECT_IN_SCENE, payload}),
  createScene: (payload) => dispatch({type: actionTypes.CREATE_SCENE, payload}),
  setActiveScene: (id) => dispatch({type: actionTypes.SET_ACTIVE_SCENE, payload: id})
});

export default connect(mapStateToProps, mapDispatchToProps)(GameScene);
