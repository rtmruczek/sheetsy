import React, { Component } from 'react';

class MouseProps extends Component {
    state = {
      x: null,
      y: null,
      backgroundDrag: {
        active: false,
        startX: 0,
        startY: 0,
        screenOffsetX: 0,
        screenOffsetY: 0,
        startScreenOffsetX: 0,
        startScreenOffsetY: 0
      }
    }
    render () {
      return this.props.children;
    }

    componentWillUnmount () {
      // document.removeEventListener()
    }

    componentDidMount () {
      document.addEventListener('mousedown', (evt) => {
        if (evt.target.tagName === 'BODY') {
          const {x, y} = evt;
          this.setState({
            backgroundDrag: {...this.state.backgroundDrag,
              ...{
                startScreenOffsetX: this.state.backgroundDrag.screenOffsetX,
                startScreenOffsetY: this.state.backgroundDrag.screenOffsetY,
                screenOffsetX: this.state.backgroundDrag.screenOffsetX,
                screenOffsetY: this.state.backgroundDrag.screenOffsetY,
                active: true,
                startX: x,
                startY: y
              }}
          });
        } else if (evt.target.className.indexOf('sceneCard') !== -1 ||
              evt.target.className.indexOf('aspect-card') !== -1) {
          // do nothing
        }
      });
      document.addEventListener('mousemove', (evt) => {
        let newState = {...this.state, ...{x: evt.x, y: evt.y}};
        if (this.state.backgroundDrag.active) {
          newState.backgroundDrag.screenOffsetX = (this.state.backgroundDrag.startScreenOffsetX + this.state.backgroundDrag.startX - evt.x);
          newState.backgroundDrag.screenOffsetY = (this.state.backgroundDrag.startScreenOffsetY + this.state.backgroundDrag.startY - evt.y);
          this.props.onMouseMoveDown(newState);
        }
        this.setState(newState);
      });

      document.addEventListener('mouseup', (evt) => {
        this.setState({
          backgroundDrag: {
            ...this.state.backgroundDrag,
            active: false
          }
        }, () => {
          this.props.onMouseUp(this.state);
        });
      });
    }
}

export default MouseProps;
