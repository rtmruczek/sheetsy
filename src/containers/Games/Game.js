import React from 'react';
import { PropTypes } from 'prop-types';
import { List } from 'semantic-ui-react';

const Game = (props) => {
  const onClick = () => {
    props.onClick(props.game.id);
  };
  return (<List.Item
    onClick={onClick}
  >
    <List.Header>
      {props.game.title}
    </List.Header>
    <List.Description>
      {props.game.description}
    </List.Description>
  </List.Item>);
};

Game.propTypes = {
  game: PropTypes.object,
  onClick: PropTypes.func
};

export default Game;
