import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Button, Divider, List } from 'semantic-ui-react';
import { values } from 'lodash';

import Game from './Game';
import * as actionTypes from '../../actionTypes';

class Games extends Component {
  static propTypes = {
    games: PropTypes.array,
    getGames: PropTypes.func,
    pushRoute: PropTypes.func
  }
  componentWillMount () {
    this.props.getGames();
  }
  gotoNew = () => {
    this.props.pushRoute('new-game');
  }
  gotoDetail = (id) => {
    this.props.pushRoute(`games/${id}`);
  }
  render () {
    return (
      <div>
        <h1>Games</h1>
        <List
          divided
          link
          selection
          size='big'
        >
          { this.props.games.length > 0
            ? values(this.props.games).map(game => (
              <Game
                onClick={this.gotoDetail}
                key={game.id}
                game={game}
              />
            ))
            : <div>
              <h3>You don't have any games yet</h3>
            </div>
          }
        </List>
        <Divider />
        <div>
          <Button
            onClick={this.gotoNew}
            primary icon='plus' content='Create game' />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  games: state.games.mine
});

const mapDispatchToProps = (dispatch) => ({
  getGames: () => dispatch({
    type: actionTypes.REQUEST_GAMES
  }),
  pushRoute: (route) => dispatch(push(route))
});
export default connect(mapStateToProps, mapDispatchToProps)(Games);
