import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class Home extends Component {
  static propTypes = {
    email: PropTypes.string
  }
  render () {
    return (
      <div style={{
        padding: 10
      }}>
        <h1>Hello {this.props.email}</h1>
        <Link to='/games'>Games</Link>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  email: state.auth.user.email
});

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
