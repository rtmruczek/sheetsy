import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { find } from 'lodash';
import { connect } from 'react-redux';
import CharacterForm from '../components/CharacterForm';

class CharacterDetail extends Component {
  static propTypes = {
    character: PropTypes.object,
    characterForm: PropTypes.object,
    updateCharacter: PropTypes.func,
    updateField: PropTypes.func
  }
  constructor (props) {
    super(props);
    this.state = {...props.character};
  }
  componentWillReceiveProps (props) {
    this.setState({
      ...this.sanitizeData(props.character),
      ...props.characterForm
    });
  }
  sanitizeData (givenState) {
    let newState = { ...givenState };
    if (!Array.isArray(newState.skills)) {
      newState = {
        ...newState,
        skills: []
      };
    }
    return newState;
  }
  updateCharacter = () => {
    this.props.updateCharacter(this.state);
  }
  render () {
    const { updateField } = this.props;
    return (
      <CharacterForm
        updateField={updateField}
        character={this.state}
        addCharacter={this.updateCharacter}
      />
    );
  }
}

const mapStateToProps = (state, routerProps) => ({
  character: find(state.characters.mine, c => c.id === routerProps.match.params.id) || {},
  characterForm: state.characters.form
});
const mapDispatchToProps = (dispatch) => ({
  updateField: (name, value) => dispatch({
    type: actionTypes.UPDATE_FIELD,
    payload: {name, value}
  }),
  updateCharacter: (character) => dispatch({
    type: actionTypes.UPDATE_CHARACTER,
    payload: character
  })
});

export default connect(mapStateToProps, mapDispatchToProps)(CharacterDetail);
