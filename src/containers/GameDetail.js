import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { find } from 'lodash';
import { push } from 'react-router-redux';
import * as actionTypes from '../actionTypes';
import { Button, Confirm, Divider } from 'semantic-ui-react';

class GameDetail extends Component {
  static propTypes = {
    currentGame: PropTypes.object,
    addScene: PropTypes.func,
    deleteGame: PropTypes.func,
    getGames: PropTypes.func,
    pushRoute: PropTypes.func
  }
  state = {open: false}

  componentWillMount () {
    this.props.getGames();
  }

  show = () => this.setState({ open: true })
  handleConfirm = () => {
    const { id } = this.props.currentGame;
    this.props.deleteGame(id);
    this.setState({ open: false });
    this.props.pushRoute('/games');
  }
  handleCancel = () => this.setState({ open: false })

  render () {
    const { currentGame } = this.props;
    if (currentGame) {
      return (
        <div>
          <div style={{
            display: 'flex',
            justifyContent: 'space-between'
          }}>
            <h1>
              { currentGame.title }
            </h1>
            <div>
              <Button
                color='red'
                icon='trash'
                onClick={this.show} />
              <Confirm
                cancelButton='No, cancel'
                confirmButton='Yes, delete'
                open={this.state.open}
                onCancel={this.handleCancel}
                onConfirm={this.handleConfirm}
              />
            </div>
          </div>
          <h2>Characters</h2>
          <Divider />
          <h2>Campaigns</h2>
          {/* <h2>Scenes</h2>
          <Button
            onClick={this.addScene}
            basic
            icon='plus'
            content='Add Scene'
            primary /> */}
          <Divider />
        </div>
      );
    }
    return null;
  }
}

const getCurrentGame = (state) => {
  if (state.router.location.pathname !== undefined) {
    return find(state.games.mine, (game) => game.id === state.router.location.pathname.split('/')[2]);
  }
  return null;
};

const mapStateToProps = (state) => ({
  games: state.games.mine,
  currentGame: getCurrentGame(state)
});

const mapDispatchToProps = (dispatch) => ({
  getGames: () => dispatch({
    type: actionTypes.REQUEST_GAMES
  }),
  deleteGame: (id) => dispatch({
    type: actionTypes.DELETE_GAME,
    payload: id
  }),
  pushRoute: (route) => dispatch(push(route))
});

export default connect(mapStateToProps, mapDispatchToProps)(GameDetail);
