import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CharacterForm from '../components/CharacterForm';

class NewCharacter extends Component {
  static propTypes = {
    characterForm: PropTypes.object,
    updateField: PropTypes.func,
    updateCharacter: PropTypes.func
  }
  addCharacter = () => {
    this.props.updateCharacter(this.props.characterForm);
  }
  render () {
    return (
      <div>
        <CharacterForm
          isNewObject
          character={this.props.characterForm}
          updateField={this.props.updateField}
          addCharacter={this.addCharacter}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  characterForm: state.characters.form
});
const mapDispatchToProps = (dispatch) => ({
  updateField: (name, value) => dispatch({
    type: actionTypes.UPDATE_FIELD,
    payload: {name, value}
  }),
  updateCharacter: (character) => dispatch({
    type: actionTypes.UPDATE_CHARACTER,
    payload: character
  })
});

export default connect(mapStateToProps, mapDispatchToProps)(NewCharacter);
