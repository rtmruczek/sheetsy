import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Button, Divider } from 'semantic-ui-react';
import { push } from 'react-router-redux';
import Character from '../components/Character';

class Characters extends Component {
  static propTypes = {
    pushRoute: PropTypes.func,
    // getCharacters: PropTypes.func,
    characters: PropTypes.array
  }
  static defaultProps = {
    characters: []
  }
  componentWillMount () {
    // this.props.getCharacters();
  }
  gotoNew = () => {
    this.props.pushRoute('new-character');
  }
  render () {
    return (
      <div>
        <h1>Characters</h1>
        <Character.Group items={this.props.characters} />
        <Divider />
        <Button
          onClick={this.gotoNew}
          primary icon='plus' content='Create character' />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  characters: state.characters.mine
});
const mapDispatchToProps = (dispatch) => ({
  pushRoute: (route) => dispatch(push(route)),
  getCharacters: () => dispatch({type: actionTypes.REQUEST_CHARACTERS})
});

export default connect(mapStateToProps, mapDispatchToProps)(Characters);
