import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Loader } from 'semantic-ui-react';

class RootContainer extends Component {
  static propTypes = {
    children: PropTypes.node,
    loggedIn: PropTypes.bool
  }
  render () {
    if (this.props.loggedIn) {
      return (
        <div className='root-container'>
          {this.props.children}
        </div>
      );
    }
    return (<div>
      <Loader content='Logging you in...' active size='massive' />
    </div>);
  }
}

export default RootContainer;
