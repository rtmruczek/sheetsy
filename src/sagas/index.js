import { takeLatest, call, put, select, all } from 'redux-saga/effects';
import { pickBy, mapValues } from 'lodash';

import { login } from '../api/auth';
import { getAllUserData, getAllMyData } from '../api/users';
import { getGamesRequest, addGameRequest, deleteGameRequest } from './games';
import { createSceneRequest, watchScenesRequest } from './scene';
import { getCharactersRequest, addCharacterRequest, watchCharactersRequest } from './characters';

function * loginRequest () {
  const result = yield call(login);
  yield put({
    type: actionTypes.LOGIN_SUCCESS,
    payload: result
  });
}

function * getAllUserDataRequest () {
  const myUserId = yield select(state => state.auth.user.uid);
  const result = yield call(getAllUserData);
  const omittedThisUser = yield mapValues(result, (value, key) => {
    return pickBy(value, (list, userId) => userId !== myUserId || key === 'users');
  });
  yield put({
    type: actionTypes.RECEIVE_ALL_USER_DATA,
    payload: omittedThisUser
  });
}

function * getAllMyDataRequest () {
  const myUserId = yield select(state => state.auth.user.uid);
  const result = yield call(getAllMyData, myUserId);
  yield put({
    type: actionTypes.RECEIVE_ALL_MY_DATA,
    payload: result
  });
}

function * loginSuccess () {
  yield all([
    call(getAllUserDataRequest),
    call(getAllMyDataRequest)
  ]);
  yield put({type: actionTypes.BOOTSTRAP_COMPLETE});
  // todo: fork a watcher here, or just keep it last, because there's a while(true) loop in here.
  // yield call(watchCharactersRequest);
}

function * createSceneSuccess () {
  yield call(watchScenesRequest);
}

export default function * mySaga () {
  yield takeLatest(actionTypes.LOGIN_REQUEST, loginRequest);

  yield takeLatest(actionTypes.REQUEST_GAMES, getGamesRequest);
  yield takeLatest(actionTypes.CREATE_NEW_GAME, addGameRequest);
  yield takeLatest(actionTypes.DELETE_GAME, deleteGameRequest);
  // yield takeLatest(actionTypes.ADD_SCENE, addSceneRequest);

  yield takeLatest(actionTypes.REQUEST_CHARACTERS, getCharactersRequest);
  yield takeLatest(actionTypes.UPDATE_CHARACTER, addCharacterRequest);

  yield takeLatest(actionTypes.CREATE_SCENE, createSceneRequest);
  yield takeLatest(actionTypes.CREATE_SCENE_SUCCESS, createSceneSuccess);

  yield takeLatest(actionTypes.REQUEST_ALL_USER_DATA, getAllUserDataRequest);
  yield takeLatest(actionTypes.LOGIN_SUCCESS, loginSuccess);
}
