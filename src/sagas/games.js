import { call, put, select } from 'redux-saga/effects';
import { getGames, addGame, deleteGame } from '../api/games';

function * getGamesRequest () {
  const userId = yield select(state => state.auth.user.uid);
  const result = yield call(getGames, userId);
  yield put({
    type: actionTypes.RECEIVE_GAMES,
    payload: result
  });
}

function * addGameRequest (action) {
  const userId = yield select(state => state.auth.user.uid);
  const result = yield call(addGame, userId, action.payload);
  yield put({
    type: actionTypes.CREATE_NEW_GAME_SUCCESS,
    payload: result
  });
}

function * deleteGameRequest (action) {
  const userId = yield select(state => state.auth.user.uid);
  const result = yield call(deleteGame, userId, action.payload);
  yield put({
    type: actionTypes.DELETE_GAME_SUCCESS,
    payload: result
  });
}

export { addGameRequest, getGamesRequest, deleteGameRequest };
