import { call, put, select, take } from 'redux-saga/effects';
import { getCharacters, addCharacter, watchCharacters } from '../api/characters';
import { createSocketChannel } from '../api/utils';

export function * getCharactersRequest () {
  const userId = yield select(state => state.auth.user.uid);
  const payload = yield call(getCharacters, userId);
  yield put({ type: actionTypes.RECEIVE_CHARACTERS, payload });
}

export function * watchCharactersRequest () {
  const userId = yield select(state => state.auth.user.uid);
  const socket = yield call(watchCharacters, userId);
  const socketChannel = yield call(createSocketChannel, socket);

  while (true) {
    const payload = yield take(socketChannel);
    yield put({ type: actionTypes.RECEIVE_CHARACTERS, payload });
  }
}

export function * addCharacterRequest (action) {
  const userId = yield select(state => state.auth.user.uid);
  const result = yield call(addCharacter, userId, action.payload);
  yield put({
    type: actionTypes.CREATE_NEW_CHARACTER_SUCCESS,
    payload: result
  });
}
