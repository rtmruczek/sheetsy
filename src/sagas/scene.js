import { call, put, select, take } from 'redux-saga/effects';
import { getScenes, addScene, watchScenes } from '../api/scene';
import { createSocketChannel } from '../api/utils';

export function * getScenesRequest () {
  const userId = yield select(state => state.auth.user.uid);
  const payload = yield call(getScenes, userId);
  yield put({ type: actionTypes.RECEIVE_SCENES, payload });
}

export function * watchScenesRequest () {
  const userId = yield select(state => state.auth.user.uid);
  const socket = yield call(watchScenes, userId);
  const socketChannel = yield call(createSocketChannel, socket);

  while (true) {
    const payload = yield take(socketChannel);
    yield put({ type: actionTypes.RECEIVE_SCENES, payload });
  }
}

export function * createSceneRequest (action) {
  const userId = yield select(state => state.auth.user.uid);
  const result = yield call(addScene, userId, action.payload);
  yield put({
    type: actionTypes.CREATE_SCENE_SUCCESS,
    payload: result
  });
}
