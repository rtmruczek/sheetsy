This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

# Sheetsy

## What is this?
A React web app that automates / makes some tasks easier for tabletop RPGs. Firebase is the "backend-as-a-service".

## What's working?

* Google login
* Creating / updating characters

## quickstart
### for npm
```
npm install
npm start
```

### for yarn
```
yarn
yarn start
```

Either of these should install the dependencies, and then open up a browser which should ask you to login with google.

